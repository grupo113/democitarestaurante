FROM glassfish:latest

ENV TZ=America/El_Salvador
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


 
WORKDIR /


# File Author / Maintainer
LABEL maintainer="Huezo <vhuezo@hightech-corp.com>"
LABEL description="Este es un Ejemplo de Dockerfile Tomcat"
MAINTAINER Huezo <vhuezo@hightech-corp.com>


COPY DemoCitaRestaurante-1.0-SNAPSHOT.war /

#RUN /usr/local/glassfish4/bin/asadmin start-domain
#RUN /usr/local/glassfish4/bin/asadmin -u admin deploy /DemoCitaRestaurante-1.0-SNAPSHOT.war

COPY start.sh /
RUN chmod +x /start.sh

EXPOSE 8080

ENTRYPOINT ["/start.sh"]
